/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 


#include <iostream>
#include <fstream>
#include "treemap_eda.h"
#include "hashmap_eda.h"
 //#include <map> 

#include "parser.h"


using namespace std;


//Funci�n para recorrer el arbol recursivamente
int recorrerArbolParaObtenerValorTest(const bintree<string> arbol, map<string, int> mapValorVariables) {
	int valorVariable = 0;
	//Si no hemos llegado a una hoja
	if (!arbol.right().empty() && !arbol.left().empty()) {
		//El operador siempre estar� en la raiz
		string operador = arbol.root();

		int valorAux;
		//Buscamos el valor del hijo izquierdo
		if (isdigit(arbol.left().root()[0])) {
			//Es un n�mero y lo convertimos a int
			valorAux = stoi(arbol.left().root());
		}
		else {
			//si no lo es, el nombre de una variable y lo cogemos del map
			valorAux = mapValorVariables[arbol.left().root()];
		}
		//Dependiendo del operador realizaremos una operaci�n u otra,
		//pero siempre del valor que nos haya devuelto la recursi�n del hijo derecho
		if (operador == "+") {
			valorVariable = valorAux + recorrerArbolParaObtenerValorTest(arbol.right(), mapValorVariables);
		}
		else if (operador == "-") {
			valorVariable = valorAux - recorrerArbolParaObtenerValorTest(arbol.right(), mapValorVariables);
		}
		else if (operador == "*") {
			valorVariable = valorAux * recorrerArbolParaObtenerValorTest(arbol.right(), mapValorVariables);
		}
	}
	else {
		//LLEGAMOS A UNA HOJA
		if (isdigit(arbol.root()[0])) {
			//Si lo es tenemos que operar
			valorVariable = stoi(arbol.root());
		}
		else {
			//si no lo es, el nombre de una variable
			valorVariable = mapValorVariables[arbol.root()];
		}
	}

	return valorVariable;



}


bool tratar_casof02test() {
	int numero_instrucciones;

	std::cin >> numero_instrucciones;
	//Leer los casos
	while (numero_instrucciones != 0) {
		map<string, int> valorVariables;



		//Leemos las instrucciones
		for (int i = 0; i < numero_instrucciones; i++) {
			//Parseo la instruccion
			bintree<string> instruccion = parse(std::cin);
			//el nombre de la variable siempre va a estar en el hijo izquierdo
			string nombreVariable = instruccion.left().root();
			//A partir de ahi, tengo que ir consultando el arbol del hijo derecho para obtener el valor recursivamente
			int valorVariable = recorrerArbolParaObtenerValorTest(instruccion.right(), valorVariables);
			//A�adimos el valor de la variable a nuestro map
			valorVariables[nombreVariable] = valorVariable;
		}

		//Recorremos el mapa y mostramos las instrucciones y su valor
		for (auto instruccion : valorVariables) {
			cout << instruccion.clave << " = " << instruccion.valor << endl;
		}
		cout << "---" << endl;


		//Se siguen pidiendo casos
		std::cin >> numero_instrucciones;
	}


	return false;
}



int mainF02test() {
#ifndef DOMJUDGE
	std::ifstream in("sampleF02.in");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_casof02test()) {}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}
*/