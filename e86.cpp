/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include "bintree_eda.h"


using namespace std;


void tratar_caso86() {
	auto arbol = leerArbol(-1);

	//Si no es un �rbol vac�o
	if (!arbol.empty()) {
	//Preparamos un recorrido por niveles
		queue<bintree<int>> colaNiveles;
		queue<int> resultado;
		int nivelActual = 1, nivelSiguiente = 0;
		//Insertamos en la cola la ra�z
		colaNiveles.push(arbol);
		//a�adimos la ra�z al resultado
		resultado.push(arbol.root());
		while (!colaNiveles.empty()) {
			//Cogemos el primero elemento de la cola
			auto nodo = colaNiveles.front();
			//y lo eliminamos
			colaNiveles.pop();
			//Restamos 1 a los nodos del nivel actual
			--nivelActual;

			//Comprobamos el hijo izquierdo
			if (!nodo.left().empty()) {
				//Se a�ade a la cola
				colaNiveles.push(nodo.left());
				//Se aumentan los nodos del nivel siguiente
				++nivelSiguiente;
			}
			//Y el derecho
			if (!nodo.right().empty()) {
				//Se a�ade a la cola
				colaNiveles.push(nodo.right());
				//Se aumentan los nodos del nivel siguiente
				++nivelSiguiente;
			}

			//Si no quedan m�s nodos que visitar en el nivel actual
			if (nivelActual == 0) {
				//cambiamos al nivel siguiente
				nivelActual = nivelSiguiente;
				//reiniciamos contador
				nivelSiguiente = 0;
				//a�adimos el primero elemento de la cola a nuestro resultado
				if (!colaNiveles.empty()) {
					//El primer elemento del nivel siguiente ser� el perfil izquierdo
					resultado.push(colaNiveles.front().root());
				}
			}
		}
		//Imprimimos el resultado
		while (!resultado.empty()) {
			cout << resultado.front() << " ";
			resultado.pop();
		}
		cout << endl;


	}
}



int main86() {
#ifndef DOMJUDGE
	//std::ifstream in("sample.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int numero_casos;
	cin >> numero_casos;
	for (int i = 0; i < numero_casos; i++) {
		tratar_caso86();
	}
	

#ifndef DOMJUDGE
	//	std::cin.rdbuf(cinbuf);
		// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}
