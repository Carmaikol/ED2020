
//MIGUEL AGUILERA ZORZO 05450952K F56


#include <iostream>
#include <fstream>
#include "buscaminas.h"


using namespace std;


bool tratar_casoF03() {
	Buscaminas buscaminas;
	string accion;
	cin >> accion;
	while (accion != "FIN") {
		if (cin.eof()) return false;
		string jugador;
		int x, y;
		if (accion == "mina") {
			cin >> x >> y;
			buscaminas.anyadir_mina(x, y);

			cout << buscaminas.num_minas() << endl;
		}
		else if (accion == "jugador") {
			string jugador;
			cin >> jugador;
			try{
			buscaminas.anyadir_jugador(jugador);
			}
			catch (invalid_argument &e) {
				cout << e.what() << endl;
			}
		
		}
		else if (accion == "explorar") {
			cin >> jugador >> x >> y;
			try{
				int minasCercanas = buscaminas.explorar(jugador, x, y);
				if (minasCercanas == -1) {
					cout << "Has perdido";
				}
				else {
					cout << minasCercanas;
					
				}
			}catch(invalid_argument& e) {
				cout << e.what();
			}
			cout << endl;
		
		}
		else if (accion == "bandera") {
			cin >> jugador >> x >> y;
			try {
				if (buscaminas.bandera(jugador, x, y)) {
					cout << "Si " << buscaminas.puntacion(jugador);
				}
				else {
					cout << "No " << buscaminas.puntacion(jugador);
				}
			}
			catch (invalid_argument & e) {
				cout << e.what();
			}
			cout << endl;

		}
		else if (accion == "bordes") {
			int max_x, max_y, min_x, min_y;

			buscaminas.bordes_tablero(min_x, min_y, max_x, max_y);

			cout << min_x << " " << min_y << " " << max_x << " " << max_y << endl;
		

		}


		cin >> accion;
		
	}

	cout << "---" << endl;
	return true;
}


int mainF03() {
#ifndef DOMJUDGE
	//std::ifstream in("sampleF01.in");
//	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_casoF03()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}