
//MIGUEL AGUILERA ZORZO 05450952K F56


#include <iostream>
#include <fstream>
#include <unordered_map>
#include <list>

using namespace std;


class Supermercado {
public:
	Supermercado(const int num_cajas) : numero_cajas(num_cajas) {}

	//O(1)
	void nuevo_cliente(const string& cod_cliente, const int& num_cola) {
		//Comprobamos la cola
		comprobarCola(num_cola);

		try {
			//Buscamos el cliente  
			Info_cliente& info = buscarCliente(cod_cliente);    //O(1)
		}
		catch (domain_error & e) {
			//Si salta excepcion significa que ya est�
			//Lo insertamos en la lista de la cola "num_cola"
			colas[num_cola].push_back(cod_cliente);				//O(1)
			//Insertamos el cliente con los datos (el iterador que apunte al elemento que se acaba de insertar)
			clientes.insert({ cod_cliente, {cod_cliente, num_cola, --colas[num_cola].end() } });  //O(1)
			//Volvemos
			return;
		}
		//Si se llega aqui, lanzamos excepci�n
		throw domain_error("Cliente duplicado");
	}


	//O(1)
	void cambiar_cola(const string& cod_cliente, const int& num_cola) {
		//Comprobamos la cola
		comprobarCola(num_cola);
		//Buscamos el cliente  
		Info_cliente& info = buscarCliente(cod_cliente);    //O(1)
		//Solo si la cola es distinta
		if (info.num_cola != num_cola) {
			//Borrar de la cola anterior
			colas[info.num_cola].erase(info.it_cola);		//O(1)
			//Introducir en la nueva cola
			colas[num_cola].push_back(info.cod_cliente);	//O(1)
			//Actualizar info de cliente
			info.num_cola = num_cola;
			info.it_cola = --colas[num_cola].end();	
		}
	}

	//O(1)
	const int consultar_cliente(const string& cod_cliente) const {
		//Buscamos el cliente  
		Info_cliente info = buscarCliente(cod_cliente);    //O(1)
		//Devolvemos el n�mero de cola
		return info.num_cola;
	}

	//O(1)
	const int cuantos_en_cola(const int& num_cola) const {
		//Comprobamos la cola
		comprobarCola(num_cola);
		int tamanyo = 0;
		if (colas.count(num_cola)) {
			tamanyo = colas.at(num_cola).size();
		}
		//Devolvemos el tama�o de la lista en la cola "num_cola"
		return tamanyo;
	}

	//O(1)
	list<string> clientes_en_cola(const int& num_cola) {
		//Comprobamos la cola
		comprobarCola(num_cola);

		list<string> resultado;

		auto it = colas[num_cola].begin();

		while (it != colas[num_cola].end()) {
			resultado.push_front(*it);
			++it;
		}

		//auto it = colas[num_cola].end();
		//while (it != colas[num_cola].begin()) {
		//}

		return resultado;
	
	
	}


private:
	//Info cliente
	struct Info_cliente {
		string cod_cliente;
		int num_cola;
		list<string>::iterator it_cola;

		Info_cliente(string cod_cliente, int num_cola, list<string>::iterator it_cola) :
			cod_cliente(cod_cliente), num_cola(num_cola), it_cola(it_cola) {}
	};
	//Diccionario de clientes,infoCliente
	unordered_map<string, Info_cliente> clientes;
	//Diccionario de colas, listaClientes
	unordered_map<int, list<string>> colas;
	//Numero cajas del supermercado
	int numero_cajas;

	//O(1)
	 void comprobarCola(const int &num_cola) const {
		if (num_cola < 0 || num_cola >= numero_cajas) {
			throw domain_error("Cola inexistente");
		}
	}

	 //O(1)
	Info_cliente& buscarCliente(const string& cod_cliente) {
		auto it = clientes.find(cod_cliente);
		if (it == clientes.end()) {
			throw domain_error("Cliente inexistente");
		}
		else {
			return it->second;
		}
	}

	//O(1)
	 const Info_cliente& buscarCliente(const string& cod_cliente) const{
		auto it = clientes.find(cod_cliente);
		if (it == clientes.end()) {
			throw domain_error("Cliente inexistente");
		}
		else {
			return it->second;
		}
	}
};



bool tratar_caso_super() {


	int numero_cajas;
	//Se pide el n�mero de cajas
	cin >> numero_cajas;
	if (cin.eof()) return false; //Si llegamos al final del fichero salimos

	//Se crea el supermercado correspondiente
	Supermercado super(numero_cajas);
	//Se pide una acci�n
	string accion;
	cin >> accion;
	while (accion != "FIN") {
	
		string cod_cliente;
		int num_cola;
		if (accion == "nuevo_cliente") {
			cin >> cod_cliente;
			cin >> num_cola;
			try {
				super.nuevo_cliente(cod_cliente, num_cola);
			}
			catch (domain_error & e) {
				cout << "ERROR: " << e.what() << endl;
			}
		}
		else if (accion == "cambiar_cola") {
			cin >> cod_cliente;
			cin >> num_cola;

			try {
				super.cambiar_cola(cod_cliente, num_cola);
			}
			catch (domain_error & e) {
				cout << "ERROR: " << e.what() << endl;
			}



		}
		else if (accion == "consultar_cliente") {
			cin >> cod_cliente;

			try {
				int numero_cola = super.consultar_cliente(cod_cliente);
				cout << "El cliente " << cod_cliente << " esta en la cola " << numero_cola << endl;
			}
			catch (domain_error & e) {
				cout << "ERROR: " << e.what() << endl;
			}


		}
		else if (accion == "cuantos_en_cola") {
			cin >> num_cola;

			try {
				int cuantos = super.cuantos_en_cola(num_cola);
				cout << "En la cola " << num_cola << " hay " << cuantos << " clientes" << endl;
				
			}
			catch (domain_error & e) {
				cout << "ERROR: " << e.what() << endl;
			}

		}
		else if (accion == "clientes_en_cola") {
			cin >> num_cola;

			try {
				list<string> clientes = super.clientes_en_cola(num_cola);
				cout << "En la cola " << num_cola << " estan:";
				for (auto cliente : clientes) {
					cout << " " << cliente;
				}
				cout << endl;

			}
			catch (domain_error & e) {
				cout << "ERROR: " << e.what() << endl;
			}


		}
		
		cin >> accion;
	}

	cout << "---" << endl;
	return true;
}


int mainF04() {
#ifndef DOMJUDGE
	//std::ifstream in("sampleF01.in");
//	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso_super()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}