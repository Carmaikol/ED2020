//Miguel Aguilera Zorzo  05450952K
//Insertar sublista en lista


#include <iostream>
#include "juntarListas.h"

using namespace std;




bool tratar_caso10() {
	int num_elems, numero, posicion;

	cin >> num_elems;
	if (cin.eof()) return false;

	juntarListas<int> lista;
	juntarListas<int> sublista;


	for (int i = 0; i < num_elems; i++) {
		cin >> numero;
		lista.push(numero);
	}

	cin >> num_elems >> posicion;

	for (int i = 0; i < num_elems; i++) {
		cin >> numero;
		sublista.push(numero);
	}

	if (!lista.empty() && !sublista.empty()) {
		lista.juntar(sublista, posicion);
	}

	if(!lista.empty())
		lista.mostrar();


	cout << endl;

	return true;
}


int main10(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso10()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}