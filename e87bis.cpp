﻿//Miguel Aguilera Zorzo  05450952K
// Camino de pares   (87)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm>

using namespace std;

void caminoParesG2(const bintree<int>& arbol, int& maxAltura, int& maxCamino)
{

	if (!arbol.empty()) {
		int nivelCombinado = 0; 
		int maxIzq, maxDer; //Recoge el maximo de los subarboles relativos
		int maxCaminoIzq, maxCaminoDer;
		//Recorremos el hijo izquierdo
		caminoParesG2(arbol.left(), maxIzq, maxCaminoIzq);
		//Recorremos el hijo derecho
		caminoParesG2(arbol.right(), maxDer, maxCaminoDer);
		//Nos interesa si es par
		if (arbol.root() % 2 == 0) {
			nivelCombinado = maxIzq + maxDer + 1; //Calculamos el camino combinado + el nodo actual
			maxAltura = 1 + max(maxDer, maxIzq); //Calculamos el maximo de losniveles

			maxCamino = max(max(maxCaminoIzq, maxCaminoDer), nivelCombinado);//Vamoscomprobando el maximo camino y lo guardamos

		}
		else {
			//Si no es par, se reinicia la cuenta de los niveles
			maxAltura = 0; //?
			maxCamino = max(maxCaminoIzq, maxCaminoDer);
		}

	}
	else {
		//LLegamos a hoja, empezamos a contar
		maxAltura = 0;
		maxCamino = 0;
	}
}
//Función para el uso del "usuario"
int caminoPares2(const bintree<int>& arbol) {
	int  maxAltura, maxCamino = 0;
	//Hacemos la llamada a la funcion general
	caminoParesG2(arbol, maxAltura, maxCamino);
	//Devolvemos el camino máximo
	return maxCamino;
}


//MAIN GENERICO
int main878(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		bintree<int> tree;
		tree = leerArbol(-1);
		cout << caminoPares2(tree) << endl;

	}
	return 0;
}