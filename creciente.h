#ifndef creciente_h
#define creciente_h

#include "queue_eda.h"
#include <iostream>
using namespace std;


template <class T>
class Creciente : public queue<T> {
public:
    void creciente();
    void mostrar();
protected:
    using Nodo = typename queue<T>::Nodo;



};



template <class T>
void Creciente<T>::creciente(){
    Nodo *actual = this->prim;
    Nodo *aBorrar;
    while(actual->sig != nullptr){
        //si el elemento siguiente es menor al actual
        if(actual->sig->elem < actual->elem){
            //Guardamos el elemento a borrar
            aBorrar = actual->sig;
            //Actualizamos el siguiente del actual
            actual->sig = actual->sig->sig;
            //Borramos el nodo
            delete(aBorrar);
            //Mantenemos el número de elementos
            this->nelems--;
        }

        actual = actual->sig;
    }
    //Actualizamos el último
    this->ult = actual;

};


template <class T>
void Creciente<T>::mostrar() {
	Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}


#endif