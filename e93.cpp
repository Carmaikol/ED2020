/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include <map>

using namespace std;

struct instruccion {
	string comando;
	int argumento;
};


bool tratar_caso93() {
	int numero_linea, contador = 10;
	map<int, instruccion> programa;
	map<int, int> traduccion;
	//Se pide el numero de l�nea
	cin >> numero_linea;
	//Hasta que sea 0
	while (numero_linea != 0) {
		instruccion instruccion;
		//Se pide el comando
		cin >> instruccion.comando;
		//Si la instrucci�n es distinto de return
		if (instruccion.comando != "RETURN") {
			//Se pide el argumento
			cin >> instruccion.argumento;
		}
		//Se inserta la instrucci�n en el programa
		programa.insert({ numero_linea,instruccion });
		//Se inserta la traduccion de la linea
		traduccion.insert({ numero_linea,contador });
		//Se aumenta el contador
		contador += 10;
		cin >> numero_linea;
	}

	//Se imprime el programa
	for (auto pair : programa) {
		cout << traduccion[pair.first] << " " << pair.second.comando;
		if (pair.second.comando != "RETURN") {
			cout << " " << traduccion[pair.second.argumento];
		}
		cout << endl;
	}
	cout << "---" << endl;


	// Implementar. Devolver false si es el
	// caso de prueba que determina el fin de entrada, 
	// o true en caso contrario.
	return false;
}



int main93() {
#ifndef DOMJUDGE
//	std::ifstream in("sample.in");
//	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif
	int numero_casos;
	cin >> numero_casos;
	for (int i = 0; i < numero_casos; i++) {
		tratar_caso93();
	}
	

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	// system("PAUSE");
#endif
	return 0;
}
