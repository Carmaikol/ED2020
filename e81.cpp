//Miguel Aguilera Zorzo  05450952K
// ARBOL SIMETRICO   (81)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm> 

using namespace std;


bool arbolSimetrico(const bintree<char>& izquierdo, const bintree<char>& derecho) {
	bool arbolOK;
	//Comprobamos los casos base (dos hijos vacios, solo hijo izquierdo, solo hijo derecho)
	//Si los dos �rboles son vac�os se rompe la simetr�a
	if (izquierdo.empty() && derecho.empty()) {
		arbolOK = true;
	}
	//Si alguno de los dos �rboles es vac�o, se romper�a la simetr�a
	else if (!izquierdo.empty() && derecho.empty()) {
		arbolOK = false;
	}
	else if (izquierdo.empty() && !derecho.empty()) {
		arbolOK = false;
	}
	//Caso general (Comprobamos los hijos de los dos sub�rboles no vac�os
	else {
		bool izquierdoOK, derechoOK;
		//Comprobamos los nodos "exteriores"
		izquierdoOK = arbolSimetrico(izquierdo.left(), derecho.right());
		//Comprobamos los nodos "interiores"
		derechoOK = arbolSimetrico(izquierdo.right(), derecho.left());
		//El arbol es correcto si ambos sub�rboles son correctos
		arbolOK = izquierdoOK && derechoOK;
	}

	return arbolOK;

}

bool arbolSimetricoG(const bintree<char>& arbol) {
	bool arbolOK;

	//Comprobamos que el arbol no es vac�o
	if (arbol.empty()) {
		arbolOK = false;
	}else {
		//Caso recursivo
		arbolOK = arbolSimetrico(arbol.left(), arbol.right());
	}	
	

	return arbolOK;
}



//MAIN GENERICO
int main81(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		int res;
		bintree<char> tree;
		tree = leerArbol('.');
		res = arbolSimetricoG(tree);
		if (res)
			cout << "SI "  << endl;
		else
			cout << "NO" << endl;
	}
	return 0;
}