//Miguel Aguilera Zorzo  05450952K
// Aguas Limpias   (82)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm>

using namespace std;


int const TRAMO_NAVEGABLE = 3;

int aguasLimpias(const bintree<int>& arbol, int& caudal) {
	int tramosNavegables = 0; //Variable de retorno
	if (!arbol.empty()) {
		// Analizamos los casos base
		//Si los dos hijos son vac�os, se trata de una hoja (el comienzo del r�o)
		if (arbol.left().empty() && arbol.right().empty()) {
			caudal = 1; // Sumamos uno al caudal
			tramosNavegables = 0;	//No hay tramos navegables
		}else
		//Hijo izquierdo no vac�o y derecho vac�o
		if (!arbol.left().empty() && arbol.right().empty()) {
			int tramosNavegablesIzquierda, caudalIzquierdo;
			//recursi�n sobre hijo izquierdo
			tramosNavegablesIzquierda = aguasLimpias(arbol.left(), caudalIzquierdo);
			//Normalizamos el caudal (no puede ser menor a 0)
			caudal = max(caudalIzquierdo - arbol.root(), 0);
			//Los tramos navegables ser�n los del hijo +  el nivel actual (si se cumple la condici�n)
			tramosNavegables = tramosNavegablesIzquierda + (caudalIzquierdo >= TRAMO_NAVEGABLE);
		}else
		//Hijo izquierdo vac�o y derecho no vac�o
		if (arbol.left().empty() && !arbol.right().empty()) {
			int tramosNavegablesDerecha, caudalDerecho;
			//recursi�n sobre hijo derecho
			tramosNavegablesDerecha = aguasLimpias(arbol.right(), caudalDerecho);
			//Normalizamos el caudal (no puede ser menor a 0)
			caudal = max(caudalDerecho - arbol.root(), 0);
			//Los tramos navegables ser�n los del hijo +  el nivel actual (si se cumple la condici�n)
			tramosNavegables = tramosNavegablesDerecha + (caudalDerecho >= TRAMO_NAVEGABLE);

		}
		//Caso general (Hijo izquierdo e hijo derecho)
		else{
			int tramosNavegablesIzquierda, caudalIzquierdo;
			int tramosNavegablesDerecha, caudalDerecho;
			//recursi�n sobre hijo izquierdo
			tramosNavegablesIzquierda = aguasLimpias(arbol.left(), caudalIzquierdo);
			//recursi�n sobre hijo derecho
			tramosNavegablesDerecha = aguasLimpias(arbol.right(), caudalDerecho);
			//sumamos los caudales de los hijos y restamos la "presa" actual
			caudal = max(caudalIzquierdo + caudalDerecho - arbol.root(), 0);
			//Sumamos los tramos navegables de los hijos + el nivel actual si se cumple la condici�n
			tramosNavegables = tramosNavegablesIzquierda + tramosNavegablesDerecha + (caudalDerecho >= TRAMO_NAVEGABLE) + (caudalIzquierdo >= TRAMO_NAVEGABLE);
		}
	}

	return tramosNavegables;
}


//MAIN GENERICO
int main82(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		int res;
		bintree<int> tree;
		int caudal, tramosNavegables;
		tree = leerArbol(-1);
		tramosNavegables = aguasLimpias(tree,caudal);
		
		cout << tramosNavegables << endl;
		
	}
	return 0;
}