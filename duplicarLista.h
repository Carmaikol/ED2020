#ifndef duplicar_lista_h
#define duplicar_lista_h


#include "queue_eda.h"
#include <iostream>

using namespace std;

template <class T>
class duplicarLista : public queue<T> {
public:
	void duplicar();
	void mostrar();
};



template <class T>
void duplicarLista<T>::duplicar() {
	queue<int>::Nodo *actual;
	actual = this->prim;

	while (actual->sig != nullptr) {
		//guardamos el siguiente
		queue<int>::Nodo* siguiente = actual->sig;
		//Se crea la copia del nodo actual con siguiente "siguiente"
		queue<int>::Nodo* nuevo = new queue<int>::Nodo(actual->elem, siguiente);
		//El siguiente del actual es el nuevo
		actual->sig = nuevo;
		//Seguimos iterando
		actual = siguiente;
	}
	//Copiamos el �ltimo elemento
	queue<int>::Nodo* ultimo = new queue<int>::Nodo(actual->elem, nullptr);
	actual->sig = ultimo;
	this->ult = ultimo;


}


template <class T>
void duplicarLista<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}










#endif