//Miguel Aguilera Zorzo  05450952K
// EXCURSIONISTAS ATRAPADOS    (23)

#include "bintree_eda.h"
#include <iostream>
#include <algorithm>  
using namespace std;


template <class T>
void rescate(const bintree<T>& arbol, int& rescates, int& maximo) {
	//Se comprueba que el arbol no es vacio
	if (arbol.empty()) {
		//Si es vac�o
		rescates = 0; //No se producen rescates
		maximo = 0;   //El m�ximo es 0
		return;
	}
	//Se recorre el arbol
	//Primero el hijo izquierdo
	int rescatesIzq, maximoIzq;
	rescate(arbol.left(), rescatesIzq, maximoIzq);
	//Despu�s el hijo derecho
	int rescatesDer, maximoDer;
	rescate(arbol.right(), rescatesDer, maximoDer);
	//Calculamos los rescates
	int rescatesRaiz = 0;
	if (arbol.root() != 0) rescatesRaiz = 1; // Vemos si se produce rescate en la raiz
	rescates = max(rescatesIzq + rescatesDer, rescatesRaiz); //Nos quedamos con el maximo de rescates
	//Calculamos el maximo de personas rescatadas
	maximo = arbol.root() + max(maximoIzq, maximoDer); //tenemos que contar la raiz mas el maximo del hijo izquierdo o el derecho
	return;

}



int main23(int argc, char* args[])
{
	int N;
	cin >> N;
	for (int n = 0; n < N; n++)
	{
		bintree<int>  arbol;
		int rescates, maximo;
		arbol = leerArbol(-1);
		rescate(arbol, rescates, maximo);
		cout << rescates << " " << maximo << endl;
	}
	return 0;
}
