//Miguel Aguilera Zorzo  05450952K
//Accidentes aereos


#include <iostream>
#include <string>
#include "deque_eda.h"
#include <vector>

using namespace std;




bool tratar_caso12() {
	int numero_alumnos;
	int salto;

	cin >> numero_alumnos >> salto;
	if (cin.eof()) return false;

	deque<int> dobleCola;


	if (numero_alumnos == 0 && salto == 0) return false;

	//guardamos los n�meros de los alumnos en una cola doble
	for (int i = 0; i < numero_alumnos; i++) {
		dobleCola.push_back(i + 1);
	}

	//iteramos hasta que s�lo quede un elemento
	while (dobleCola.size() > 1) {
		//tantas veces como sea la longitud del salto
		for (int i = 0; i < salto; i++) {
			//Ponemos el primero elemento al final
			dobleCola.push_back(dobleCola.front());
			//Y lo eliminamos del principio
			dobleCola.pop_front();
		}
		//El primero de la cola se salva
		dobleCola.pop_front();
	}

	cout << dobleCola.front() << endl;

	return true;
}


int main12(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso12()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}