#ifndef dos_en_dos_h
#define dos_en_dos_h
#include "queue_eda.h"
#include <iostream>

template <class T>
class dosEnDos : public queue<T> {
protected:
	
public:
	void intercambiar();
	void mostrar();
};


template <class T>
void dosEnDos<T>::intercambiar() {
	queue<int>::Nodo* actual = this->prim;
	queue<int>::Nodo* anterior = nullptr;
	while (actual != nullptr && actual->sig != nullptr) {
		//Guardo el nodo siguiente
		queue<int>::Nodo* aux = actual->sig;
		//El siguiente del actual es el siguiente del siguiente
			actual->sig = actual->sig->sig;
		//El siguiente del que hemos guardado antes es el actual
			aux->sig = actual;
		//Si el actual era el primero
			if(actual==this->prim){
				//Cambiar por el auxiliar
				this->prim = aux;
			}
		//Si el auxiliar es el ultimo
			if (aux == this->ult) {
			//El actual pasa a ser el ultimo
				this->ult = actual;
			}

			if (anterior != nullptr) {
				anterior->sig = aux;
			}
			anterior = actual;
			
			//Pasamos a la siguiente iteración
			actual = actual->sig;
		}

	if (actual != nullptr) {
		this->ult = actual;
	}
}


template  <class T>
void dosEnDos<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		std::cout << actual->elem << " ";
		actual = actual->sig;
	}


}



#endif