//Miguel Aguilera Zorzo  05450952K
// Paréntesis equilibrados


#include <iostream>
#include <stack>
#include <string>

using namespace std;


//METODOS AUXILIARES
//Ver si es un paréntesis abierto
bool abierto(const char caracter) {
	return caracter == '(' || caracter == '[' || caracter == '{';
}
//Ver si es un paréntesis cerrado
bool cerrado(const char caracter) {
	return caracter == ')' || caracter == ']' || caracter == '}';
}
//Ver si coinciden dos paréntesis
bool seCierran(const char abierto, const char cerrado) {
	bool resultado = false;
	if ((abierto == '(' && cerrado == ')') || (abierto == '[' && cerrado == ']') || (abierto == '{' && cerrado == '}')) {
		resultado = true;
	}
	return resultado;
}


bool tratar_caso06() {
	string linea;
	stack<char> pila;
	string resultado = "SI";

	getline(cin, linea);
	//cin.ignore();
	
	if (cin.eof()) return false;
	

	for (auto caracter : linea) {
		if (abierto(caracter)) {
			pila.push(caracter);
		}
		else if (cerrado(caracter)) {
			if (pila.empty()) {
				resultado = "NO";
				break;
			}

			char aComparar = pila.top();
			if (seCierran(aComparar, caracter)) {
				pila.pop();			
			}
			else {
				resultado = "NO";
				break;
			}
		}
	}

	if (!pila.empty()) resultado = "NO";

	cout << resultado << endl;

	return true;
}


int main06(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso06()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}