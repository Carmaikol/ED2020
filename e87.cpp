//Miguel Aguilera Zorzo  05450952K
// Camino de pares   (87)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm>

using namespace std;

//Funci�n general para las llamadas recursivas.
void caminoParesG(const bintree<int>& arbol, int& nivel, int& maxAltura, int& maxCamino) {
	 
	if (!arbol.empty()) {
		int nivelIzq, nivelDer, nivelCombinado = 0;  //Controla los niveles actuales
		int maxIzq, maxDer; //Recoge el m�ximo de los sub�rboles relativos
		int maxCaminoIzq, maxCaminoDer;
		//Recorremos el hijo izquierdo
		caminoParesG(arbol.left(), nivelIzq, maxIzq, maxCaminoIzq);
		//Recorremos el hijo derecho
		caminoParesG(arbol.right(), nivelDer, maxDer, maxCaminoDer);
		//Nos interesa si es par
		if (arbol.root() % 2 == 0) {
			nivel = max(nivelIzq, nivelDer) + 1; //Cogemos el m�ximo de los caminos hasta el nivel actual y sumamos el nivel actual
			nivelCombinado = nivelIzq + nivelDer + 1; //Calculamos el camino combinado + el nodo actual
			maxAltura = max(max(maxDer, maxIzq),nivel); //Calculamos el m�ximo de los niveles
			maxCamino = max(max(maxAltura, nivelCombinado), maxCamino);//Vamos comprobando el m�ximo camino y lo guardamos
			//Es importante que maxAltura y maxCamino no se lleven en la misma variable,
			// ya que al contar el camino combinado, la cuenta se debe "reiniciar".
		}
		else {
			//Si no es par, se reinicia la cuenta de los niveles
			nivel = 0;
			maxAltura = 0; // Corta la altura m�xima encontrada. Creo que no hace falta
			
		}
		maxCamino = max(max(maxCaminoIzq, maxCaminoDer), maxCamino);
	}
	else {
		//LLegamos a hoja, empezamos a contar
		nivel = 0;
		maxAltura = 0;
	}
	return;
}

//Funci�n para el uso del "usuario"
int caminoPares(const bintree<int>& arbol){
	int nivel, maxAltura, maxCamino = 0;
	//Hacemos la llamada a la funcion general
	caminoParesG(arbol, nivel, maxAltura, maxCamino);
	//Devolvemos el camino m�ximo
	return maxCamino;
}


//MAIN GENERICO
int main87(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		bintree<int> tree;
		tree = leerArbol(-1);
		cout << caminoPares(tree) << endl;

	}
	return 0;
}