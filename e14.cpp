//Miguel Aguilera Zorzo  05450952K
// Anacleto agente secreto


#include <iostream>
#include <stack>
#include <string>

using namespace std;


bool esVocal(const char caracter)
{
  char a=tolower(caracter);
  return (a=='a' || a=='e' || a=='i' || a=='o' || a=='u');
}


bool tratar_caso14() {
	string linea;
    string resultado;
    //std::deque<char> resultado;
    std::deque<char> primerPaso;
    std::deque<char> porDetras;
	stack<char> pila;
	

	getline(cin, linea);
	//cin.ignore();
	
	if (cin.eof()) return false;
	
    //X'' -> X'
    int contador = 1;
    //recorremos la línea
	for (auto caracter : linea) {
        //Si es posición par
        if(contador % 2 == 0){
            //añadimos a porDetras
            porDetras.push_back(caracter);
        }else{
            //Si no a la cola primerPaso
            primerPaso.push_back(caracter);
        }
        contador++;
	}

    while(!porDetras.empty()){
        //Cogemos el último elemento
        char elemento = porDetras.back();
        //Lo añadimos al final
        primerPaso.push_back(elemento);
        //lo quitamos de porDetras
        porDetras.pop_back();
    }

    //X' -> X
    while(!primerPaso.empty()){
        char elemento = primerPaso.front();
        //Si no es vocal
        if(!esVocal(elemento)){
            //Añadimos a la pila el elemento
            pila.push(elemento);
        }else{
            //En cc, mientras la pila no vacía
            while(!pila.empty()){
                //añadimos la cima al resultado
                //resultado.push_back(pila.top());
                resultado += pila.top();
                pila.pop();
            }
            //añadimos el elemento al resultado
            //resultado.push_back(elemento);
            resultado += elemento;

        }
        //Iteramos
        primerPaso.pop_front();
    }



	

	cout << resultado << endl;

	return true;
}


int main(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso14()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}