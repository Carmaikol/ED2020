//Miguel Aguilera Zorzo  05450952K
// SUBARBOL COMPLETO M�S GRANDE   (83)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm>

using namespace std;

void subarbolCompleto(const bintree<char>& arbol, int& nivel, int& maxAltura) {
		//Comprobamos que �rbol no vac�o
		if (!arbol.empty()) {
			int nivelIzq, nivelDer; //Controla los niveles en los que el subarbol sigue siendo completo
			int maxIzq, maxDer; //Recoge el m�ximo de los sub�rboles relativos
			//Recorremos el hijo izquierdo
			subarbolCompleto(arbol.left(), nivelIzq, maxIzq);
			//Recorremos el hijo derecho
			subarbolCompleto(arbol.right(), nivelDer, maxDer);
			nivel = min(nivelIzq, nivelDer) + 1; //Cogemos el m�nimo de los hijos m�s el nivel actual
			maxAltura = max(max(maxDer, maxIzq),nivel); //Cogemos el m�ximo de los sub�rboles relativos
													    // y Comprobamos que el �ltimo nivel no es m�ximo
			
		}
		else {
			//Si algun hijo es vac�o, se reinicia la cuenta
			nivel = 0;
			maxAltura = 0;
		}
		
	
	

	return;
}


//MAIN GENERICO
int main83(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		
		bintree<char> tree;
		int nivel, maxAltura;
		tree = leerArbol('.');
		subarbolCompleto(tree, nivel, maxAltura);
		cout << maxAltura << endl;

	}
	return 0;
}