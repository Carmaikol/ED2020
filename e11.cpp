//Miguel Aguilera Zorzo  05450952K
//Insertar sublista en lista


#include <iostream>
#include "mergeLista.h"

using namespace std;




bool tratar_caso11() {
	int num_casos, numero;

	cin >> num_casos;
	if (cin.eof()) return false;



	while (num_casos != 0) {
		mergeLista<int> lista;
		mergeLista<int> sublista;
		cin >> numero;
		while (numero != 0) {
			lista.push(numero);
			cin >> numero;
		}

		cin >> numero;
		while (numero != 0) {
			sublista.push(numero);
			cin >> numero;
		}
	
	
		if (!lista.empty() || !sublista.empty()) {
			lista.mergear(sublista);
		}


		if (!lista.empty())
			lista.mostrar();

		cout << endl;

		num_casos--;
	
	}


	





	return true;
}


int main11(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso11()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}