#ifndef buscaminas_h
#define buscaminas_h


//MIGUEL AGUILERA ZORZO 05450952K F56


#include <string> 
#include <iomanip> 
#include <algorithm> 
#include <stdexcept> 

#include "hashmap_eda.h"
#include "set_eda.h"


using namespace std;

//template <class T, class Comparator = std::less<T>>
//class SetPosicion : public set<T, Comparator> {


//};







class Buscaminas {

public:

	//O(1)
	Buscaminas() {
		this->numero_minas = 0;
		this->miny = 99999999;
		this->minx = 99999999;
		this->maxy = -99999999;
		this->maxx = -99999999;
	}

	//O(1)
	void anyadir_mina(const int &x, const int &y) {
		if (!tablero.count({ x,y })) {
			Posicion posicion = { x,y };
			tablero.insert(posicion);  // O(1)
			numero_minas++;
			//Controlamos el rectangulo 
			if (x >= maxx) this->maxx = x;
			if (x <= minx) this->minx = x;
			if (y >= maxy) this->maxy = y;
			if (y <= miny) this->miny = y;

		}
	
	}

	//O(1)
	void anyadir_jugador(const std::string& jugador) {
		
		bool creado = false;
		try{
			int puntacion = buscar_jugador(jugador);  //O(1)
		}
		catch (invalid_argument e) {
			creado = true;
			jugadores.insert({ jugador,0 });           //O(1)
		}
		
		if (!creado)
			throw invalid_argument("Jugador existente");
		
	}

	//O(log N)
	int explorar(const std::string& jugador, const int& x, const int& y) {

		int puntacion = buscar_jugador(jugador);

		bool hayMina = tablero.count({ x,y });


		if (hayMina) {
			//Eliminar jugador
			jugadores.erase(jugador); //O(log N)
			return -1;
		}
		else {
			return calcularCasillasCercanas(x, y);
		}
	}

	//O(1)
	bool bandera(const std::string& jugador, const int& x, const int& y) {
		int &puntuacion = buscar_jugador(jugador);
		bool hayMina = tablero.count({ x,y });
		if (hayMina) {
			puntuacion++;
			return true;
		}
		else {
			puntuacion -= 5;
			puntuacion = max(0, puntuacion);
			return false;
		}
	}

	//O(1)
	int puntacion(const std::string& jugador) const {
		int puntuacion = buscar_jugador(jugador);
		return puntuacion;
	}

	//O(1)
	int num_minas() {
		return numero_minas;
	}

	// O(1)
	void bordes_tablero(int& min_x, int& min_y, int& max_x, int& max_y) {
		if (numero_minas == 0) throw std::domain_error("Tablero vacio");
		
		min_x = minx;
		min_y = miny;
		max_x = maxx;
		max_y = maxy;

	}


private:
	struct Posicion {
		int x;
		int y;

		bool operator<(const Posicion& pos2) const {

			if (x == pos2.x) {
				return y < pos2.y;
			}
			else if(x < pos2.x) {
				return true;
			
			}
			else {
				return false;
			}


		//	return ((x < pos2.x) && (y < pos2.y) ) || (x == pos2.x && y < pos2.y);
		}

		bool operator==(const Posicion& pos2) const {
			return (x == pos2.x) && (y == pos2.y);
		}
	};

	struct ComparadorPosicion{
		bool operator() (const Posicion& posicion1, const Posicion& posicion2) const {
			return (posicion1.x < posicion2.x) && (posicion1.y < posicion2.y);

		}
	
	};
	

	//Mapa para el tablero opcion1
	//unordered_map<int, unordered_map<int,bool>> tablero2;
	//set<Posicion, ComparadorPosicion> tablero;
	set<Posicion> tablero;
	//Mapa para el tablero opcion2
	//unordered_map<Posicion, bool> tablero;
	//mapa para los jugadores (hashmap)
	unordered_map<std::string, int> jugadores;
	//Variable para llevar la cuenta del n�mero de minas
	int numero_minas;


	//GUARDAR MIN X; MIN Y; MAX X; MAX Y y actualizarlo cada vez que se a�ade una mina???
	int miny, minx, maxy, maxx;

	//0(1)
	int& buscar_jugador(const std::string& jugador) {
		auto it = jugadores.find(jugador);
		if (it == jugadores.end()) {
			throw invalid_argument("Jugador no existente");
		}
		else {
			return it->valor;
		}

	}
	//0(1)
	const int& buscar_jugador(const std::string& jugador) const {
		auto it = jugadores.find(jugador);
		if (it == jugadores.end()) {
			throw invalid_argument("Jugador no existente");
		}
		else {
			return it->valor;
		}

	}

	//O(N)?�??
	int calcularCasillasCercanas(const int& x, const int& y) {

		int minas_adyacentes = 0;

		minas_adyacentes = tablero.count({ x - 1,y - 1 }) + tablero.count({ x - 1 ,y }) + tablero.count({ x - 1 , y + 1 }) + tablero.count({ x,y - 1 })
			+ tablero.count({ x,y + 1 }) + tablero.count({ x + 1,y - 1 }) + tablero.count({ x + 1 , y }) + tablero.count({ x + 1 ,y + 1 });



		/*
		if (tablero.count({ x - 1,y - 1 })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x - 1 ,y })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x - 1 , y + 1 })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x,y - 1 })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x,y + 1 })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x + 1,y - 1 })) {
			minas_adyacentes += 1;
		}

		if (tablero.count({ x + 1 , y })) {
			minas_adyacentes += 1;
		}



		if (tablero.count({ x + 1 ,y + 1 })) {
			minas_adyacentes += 1;
		}

		*/
		


		return minas_adyacentes;

	}

};



















#endif
