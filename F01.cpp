/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include "treemap_eda.h"
#include "hashmap_eda.h"
//#include <map> 

#include "list_eda.h"


using namespace std;


bool tratar_casoF01() {
	int numero_lineas;

	cin >> numero_lineas;

	//Leer los casos
	while (numero_lineas != 0) {
		unordered_map<int, list<string>> mapNumeros;
		unordered_map<string, int> numerosRestantes; //N�meros restantes para cada nombre
		//list<string> nombresEnNumero;	//Lista de nombres en cada n�mero
		//Leemos los participantes
		for(int i=0; i < numero_lineas;i++){
			string nombre;
			int numero;
			//Leemos el nombre
			cin >> nombre;
			//empezamos a leer los n�meros
			cin >> numero;
			while (numero != 0) {
				//Insertar el nombre
				mapNumeros[numero].push_back(nombre);
				//Se suman los n�meros que hay en cada nombre
				numerosRestantes[nombre]++;
				cin >> numero;
			}
		}
		//Vamos leyendo los numeros que salen
		int numeroPremiado;
		bool hayGanador = false;
		//list<string> ganadores;
		map<string, bool> ganadores;

		cin >> numeroPremiado;
		//Mientras no haya ganador
		while (!hayGanador) {
			//Comprobar los n�meros y que nombres lo contienen
		//	if (mapNumeros.count(numeroPremiado)) {
				if (!mapNumeros[numeroPremiado].empty()) {
					//Recorremos los nombres
					for (string nombre : mapNumeros[numeroPremiado]) {
						//Cuando se termina de recorrer el carton, se comprueba su tama�o
						if (--numerosRestantes[nombre] == 0) {
							//Si no hay m�s numeros, se a�ade a la lista de ganadores
							ganadores[nombre] = true;
						//	ganadores.push_back(nombre);
							//Se marca que existe un ganador
							hayGanador = true;
						}
					}
				}
		//	}
			if(!hayGanador)
			cin >> numeroPremiado;
		}
		
		//Mostramos los ganadores
		for (auto nombre : ganadores) {
			cout << nombre.clave << " ";
			//cout << nombre << " ";
		}
		cout << "\n";


		//Se siguen pidiendo casos
		cin >> numero_lineas;
	}






	// Implementar. Devolver false si es el
	// caso de prueba que determina el fin de entrada, 
	// o true en caso contrario.
	return false;
}



int mainF01() {
#ifndef DOMJUDGE
	std::ifstream in("sampleF01.in");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_casoF01()) {}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
    system("PAUSE");
#endif
	return 0;
}
