//Miguel Aguilera Zorzo  05450952K
// VIAJE A LA LUNA    (16)



#include "viajeLuna.h"
#include "Persona.h"
#include <iostream>

using namespace std;


int main16(int argc, char* args[])
{
	int N, edadMinima, edadMaxima;
	//datos de entrada
	cin >> N >> edadMinima >> edadMaxima;
	while (N != 0) {
		viajeLuna<Persona> lista;
		//recorremos los casos
		for (int i = 0; i < N; i++) {
			string nombre;
			int edad;
			cin >> edad;
			getline(std::cin, nombre); //no se puede hacer con cin por los espacios

			lista.push_back(Persona(nombre, edad));
		}
		//Utilizamos el m�todo para eliminar los que no cumplen la condici�n
		lista.remove_if([edadMinima, edadMaxima](Persona& persona) {
			//Funci�n lambda
			return !(edadMinima <= persona.getEdad() && edadMaxima >= persona.getEdad());
			});

		//recorremos los restantes para mostrar el resultado
		for (auto it = lista.cbegin(); it != lista.cend(); ++it) {
			cout << it->getNombre() << endl;
		}
		//cada caso de prueba termina con tres guiones
		cout << "---" << endl;
		cin >> N >> edadMinima >> edadMaxima;
		
	}
		
	return 0;
}