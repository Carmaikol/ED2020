/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Informática
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include "deDosEnDos.h"


using namespace std;


bool tratar_caso70() {
	int numero_elementos;
	cin >> numero_elementos;
	
	if (numero_elementos == 0) return false;

	dosEnDos<int> colaInicial;
	for (int i = 0; i < numero_elementos; i++) {
		int elemento;
		cin >> elemento;
		colaInicial.push(elemento);
	}

	colaInicial.intercambiar();
	colaInicial.mostrar();

	cout <<  endl;


	return true;
}



int main70() {
#ifndef DOMJUDGE
	//std::ifstream in("sample.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso70()) {}

#ifndef DOMJUDGE
	//	std::cin.rdbuf(cinbuf);
		// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}
