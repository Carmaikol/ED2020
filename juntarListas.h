#ifndef juntar_listas_h
#define juntar_listas_h

#include "queue_eda.h"
#include <iostream>

using namespace std;

template <class T>
class juntarListas : public queue<T> {
public:
	void juntar(juntarListas<T>& sublista, int& posicion);
	void mostrar();
};

template <class T>
void juntarListas<T>::juntar(juntarListas<T>& sublista, int& posicion) {
	queue<int>::Nodo *actual;
	queue<int>::Nodo *siguiente;
	queue<int>::Nodo *aux;
	actual = this->prim;
	int iteracion = 1;
	if (posicion == 0) {
		this->prim = sublista.prim;
		siguiente = actual;
	}
	else {	
		//Llegamos hasta la posici�n en la que hay que insertar
		while (iteracion < posicion) {
			actual = actual->sig;
			iteracion++;
		}
	

	//Guardamos el elemento siguiente a la actual, que ser� el �ltimo elemento de la sublista
	siguiente = actual->sig;
	//Guardamos el elemento
	aux = actual;
	//Pasamos a la siguiente lista
	actual = sublista.prim;
	//Actualizamos el siguiente
	aux->sig = actual;
	}
	//Borramos el prim de la sublista
	sublista.prim = nullptr;
	//while (actual->sig != nullptr) {
	//	actual = actual->sig;
	//}
	actual = sublista.ult;
	actual->sig = siguiente;
	sublista.ult = nullptr;

}



template <class T>
void juntarListas<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}












#endif