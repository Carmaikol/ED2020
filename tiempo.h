#ifndef _TIEMPO_					
#define _TIEMPO_

#include <iostream>  
#include <string> 
#include <iomanip> 
#include <algorithm> 
#include <stdexcept> 

using namespace std;


class Tiempo {
	int horas;
	int minutos;
	int segundos;

public:
	Tiempo(const int horas, const int minutos, const int segundos) :
		horas(horas), minutos(minutos), segundos(segundos)
	{
		//Se comprueba la correcci�n de las horas
		if (!(0 <= horas && horas < 24)) throw invalid_argument("");
		//Se comprueba la correcci�n de los minutos
		if (!(0 <= minutos && minutos < 60)) throw invalid_argument("");
		//Se comprueba la correcci�n de los segundos
		if (!(0 <= segundos && segundos < 60))  throw invalid_argument("");
	}

	Tiempo() : horas(0), minutos(0), segundos(0) {  }


	bool operator<(const Tiempo& tiempo2) {
		//Primero se comparan las horas
		if (horas < tiempo2.horas) return true;
		//Si son iguales se comparan los minutos
		if ((horas == tiempo2.horas) && (minutos < tiempo2.minutos)) return true;
		//si son iguales se comparan los segundos
		if ((horas == tiempo2.horas) && (minutos == tiempo2.minutos) && (segundos < tiempo2.segundos)) return true;
		//Si ning�n caso anterior se cumple la respuesta es negativa
		return false;
	};


	bool operator==(Tiempo tiempo2) const
	{
		//Se comparan las horas, los minutos y los segundos por separado
		return (horas == tiempo2.horas) && (minutos == tiempo2.minutos) && (segundos == tiempo2.segundos);
	}


	Tiempo operator+(const Tiempo& tiempo2) const
	{
		Tiempo aux = Tiempo();
		int aux_segundos, aux_minutos, aux_horas;

		//Sumamos los segundos
		aux_segundos = segundos + tiempo2.segundos;
		// Sumamos lo minutos, se deben sumar los minutos resultantes de los segundos sumados anteriormente
		aux_minutos = minutos + tiempo2.minutos + aux_segundos / 60;
		//Se suman las horas, mas los minutos  de la suma anterior
		aux_horas = horas + tiempo2.horas + aux_minutos / 60;

		//Normalizamos el tiempo
		aux.segundos = aux_segundos % 60;	//60 segundos
		aux.minutos = aux_minutos % 60;		//60 minutos
		aux.horas = aux_horas % 24;			//24 horas

		if (aux_horas > 23)  throw invalid_argument("");
		return aux;
	}

	friend istream& operator>>(istream& stream, Tiempo& Tiempo)
	{
		//Primero se introducen las horas
		stream >> Tiempo.horas;
		//saltamos los dos puntos
		stream.ignore(1, ':');
		//Luego los minutos
		stream >> Tiempo.minutos;
		//Saltamos los dos puntos
		stream.ignore(1, ':');
		//Luego los segundos
		stream >> Tiempo.segundos;

		//Comprobamos que es una hora correcta
		if (!(0 <= Tiempo.horas && Tiempo.horas < 24
			&& 0 <= Tiempo.minutos && Tiempo.minutos < 60
			&& 0 <= Tiempo.segundos && Tiempo.segundos < 60)) throw invalid_argument("");

		return stream;
	};

	friend ostream& operator<<(ostream& stream, Tiempo Tiempo)
	{
		//Para mostrarlos, setfill rellena con 0's y la de cada parte es 2
		stream << setfill('0') << setw(2) << Tiempo.horas;
		stream << ":" << setfill('0') << setw(2) << Tiempo.minutos;
		stream << ":" << setfill('0') << setw(2) << Tiempo.segundos;
		return stream;
	};

};


#endif
