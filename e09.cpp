//Miguel Aguilera Zorzo  05450952K
//Duplicar una lista enlazada


#include <iostream>
#include "invertirLista.h"

using namespace std;




bool tratar_caso09() {
	int numeros;

	cin >> numeros;
	if (cin.eof()) return false;

	invertirLista<int> lista;

	while (numeros != 0) {
		lista.push(numeros);
		cin >> numeros;
	}


	if (!lista.empty()) {
		lista.invertir();
		lista.mostrar();
	}


	cout << endl;

	return true;
}


int main09(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso09()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}