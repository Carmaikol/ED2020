//Miguel Aguilera Zorzo  05450952K
//Accidentes aereos


#include <iostream>
#include <string>
#include "deque_eda.h"
#include <vector>

using namespace std;




bool tratar_caso13() {
	int numero_elementos;
	int elemento;

	cin >> numero_elementos;
	if (cin.eof()) return false;

	deque<int> dobleCola;
	deque<int> colaAux;

	if (numero_elementos == 0) return false;
	//Recogemos los elementos
	for (int i = 0; i < numero_elementos; i++) {
		cin >> elemento;
		dobleCola.push_back(elemento);
	}

	//Recorremos la cosa
	while (dobleCola.size() != 0) {
		//cogemos el valor
		int valor = dobleCola.front();
		//Si es positivo
		if (valor >= 0) {
			//se a�ade por detr�s
			colaAux.push_back(valor);
		}
		else {
			//Si es negativo, se a�ade por delante
			colaAux.push_front(valor);
		}
		//Quitamos el primer elemento
		dobleCola.pop_front();
	}

	//Recorremos la cola aux para mostrar el resultado
	while (colaAux.size() != 0) {
		cout << colaAux.front() << " ";
		colaAux.pop_front();
	}
	cout << endl;

	return true;
}


int main13(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso13()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}