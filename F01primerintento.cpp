/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include <map> 

#include "list_eda.h"


using namespace std;


bool tratar_casoaaaaa() {
	int numero_lineas;
	map<string, list<int>> bingo;
	map<int, list<string>> mapNumeros;
	list<string> nombresEnNumero;


	cin >> numero_lineas;

	//Leer los casos
	while (numero_lineas != 0) {
		//Leemos los participantes
		for (int i = 0; i < numero_lineas; i++) {

			string nombre;
			int numero;
			list<int> carton;

			//Leemos el nombre
			cin >> nombre;
			//empezamos a leer los n�meros
			cin >> numero;
			while (numero != 0) {
				carton.push_back(numero);
				//new list<string>(nombresEnNumero);
				//Comprobar que no existe una lista ya de nombres
				if (mapNumeros.count(numero))
					nombresEnNumero = mapNumeros.at(numero);
				else
					nombresEnNumero = {};

				//Insertar el nombre
				nombresEnNumero.push_back(nombre);
				//A�adir la lista al mapa de numeros
				mapNumeros.insert({ numero, nombresEnNumero });

				cin >> numero;
			}
			bingo.insert({ nombre,carton });
		}
		//Vamos leyendo los numeros que salen
		int numeroPremiado;
		bool hayGanador = false;
		list<string> ganadores;

		cin >> numeroPremiado;
		//Mientras no haya ganador
		while (!hayGanador) {
			//Comprobar los n�meros y que nombres lo contienen
			if (mapNumeros.count(numeroPremiado)) {
				nombresEnNumero = mapNumeros.at(numeroPremiado);
				if (!nombresEnNumero.empty()) {
					//Recorremos los nombres
					for (string nombre : nombresEnNumero) {
						list<int> carton;
						//cogemos el carton
						carton = bingo.at(nombre);
						int cont = 0;
						//Recorremos el carton desde el principio
						for (auto it = carton.begin(); it != carton.end(); ) {
							//Si encontramos el numero
							int numeroAComprobar = carton.at(cont);
							if (numeroAComprobar == numeroPremiado) {
								//borramos el numero del carton
								carton.erase(it);
								//ponemos el iterador al final para terminar el bucle
								it = carton.end();
							}
							else {
								//Si no seguimos recorriendo la lista
								++it;
							}
							cont++;
						}
						//Cuando se termina de recorrer el carton, se comprueba su tama�o
						if (carton.size() == 0) {
							//Si no hay m�s numeros, se a�ade a la lista de ganadores
							ganadores.push_back(nombre);
							//Se marca que existe un ganador
							hayGanador = true;
						}
					}
				}
			}
			cin >> numeroPremiado;
		}
		//Mostramos los ganadores
		for (auto nombre : ganadores) {
			cout << nombre << " ";
		}
		cout << "\n";


		//Se siguen pidiendo casos
		cin >> numero_lineas;
	}






	// Implementar. Devolver false si es el
	// caso de prueba que determina el fin de entrada, 
	// o true en caso contrario.
	return false;
}



int mainfail() {
#ifndef DOMJUDGE
	std::ifstream in("sample.in");
	auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_casoaaaaa()) {}

#ifndef DOMJUDGE
	std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}
