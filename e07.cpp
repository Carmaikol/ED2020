//Miguel Aguilera Zorzo  05450952K
//Accidentes aereos


#include <iostream>
#include <string>
#include <stack>
#include <vector>

using namespace std;

typedef pair<string, int> elemento;


bool tratar_caso07() {
	int num_casos;

	cin >> num_casos;
	if (cin.eof()) return false;

	//vector<elemento> lista;
	stack<elemento> pila;

	string fecha;
	int victimas;
	for (int i = 0; i < num_casos; i++) {
		cin >> fecha >> victimas;
		//a�adimos a la lista
		//lista.push_back({ fecha,victimas });

		//Mientras la pila no est� vac�a y no se encuentre elemento mayor
		while (!pila.empty() && pila.top().second <= victimas) {
			//Quitar el elemento de la cima de la pila
			pila.pop();
		}
		//Si la pila no est� vac�a, el resultado es la cima
		string resultado;
		if (pila.empty()) resultado = "NO HAY";
		else resultado = pila.top().first;
		cout << resultado << endl;

		//Se a�ade la iteraci�n actual a la pila
		pila.push({ fecha,victimas });

	}

	cout << "---" << endl;

	return true;
}


int main07(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso07()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}