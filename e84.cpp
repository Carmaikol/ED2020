//Miguel Aguilera Zorzo  05450952K
// ARBOL BINARIO COMPLETO O SEMICOMPLETO   (84)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm>

using namespace std;

string esCompletoOSemicompleto(const bintree<char>& arbol, int& altura) {

	string esCompleto;
	//Comprobamos que �rbol no vac�o
	if (!arbol.empty()) {
		int nivelIzq, nivelDer; //Controla los niveles en los que el subarbol sigue siendo completo
		string esCompletoIzq, esCompletoDer; //Guarda si los subarboles son completos, semicompletos o nada
		//Recorremos el hijo izquierdo
		esCompletoIzq = esCompletoOSemicompleto(arbol.left(), nivelIzq);
		//Recorremos el hijo derecho
		esCompletoDer = esCompletoOSemicompleto(arbol.right(), nivelDer);
		altura = max(nivelIzq, nivelDer) + 1; //Calculamos la altura m�xima de los hijos m�s el nivel actual
		//Controlamos los casos en los que puede ser completo o semicompleto
		//En caso de que los dos sub�rboles sean completos
		if (esCompletoIzq == "COMPLETO" && esCompletoDer == "COMPLETO") {
			//Comprobamos las alturas para ver si este �rbol es completo o semicompleto
			if (nivelIzq == nivelDer) esCompleto = "COMPLETO";
			else if (nivelIzq == nivelDer + 1) esCompleto = "SEMICOMPLETO";
			else esCompleto = "NADA";
		}
		//En caso de que el arbol izquierdo sea completo y el derecho semicompleto
		else if (esCompletoIzq == "COMPLETO" && esCompletoDer == "SEMICOMPLETO") {
			//Tenemos que comprobar los niveles para ver si sigue siendo SEMICOMPLETO
			if (nivelIzq == nivelDer) esCompleto = "SEMICOMPLETO";
			else esCompleto = "NADA";
		}
		//En caso de que el arbol izquierdo sea semicompleto y el derecho completo
		else if (esCompletoIzq == "SEMICOMPLETO" && esCompletoDer == "COMPLETO") {
			//Tenemos que comprobar los niveles para ver si sigue siendo SEMICOMPLETO
			if (nivelIzq == nivelDer + 1) esCompleto = "SEMICOMPLETO";
			else esCompleto = "NADA";
		}
		else {
			//Todos los demas casos nos dicen que los arboles no son ni completos ni semicompletos
			esCompleto = "NADA";
		}
	}
	else {
		//Cuando llegamos a una hoja empezamos a contar niveles
		altura = 0;
		esCompleto = "COMPLETO";
	}


	return esCompleto;
}


//MAIN GENERICO
int main84(int argc, char** args)
{
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{

		bintree<char> tree;
		int altura;
		tree = leerArbol('.');
		cout << esCompletoOSemicompleto(tree, altura) << endl;

	}
	return 0;
}