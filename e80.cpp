//Miguel Aguilera Zorzo  05450952K
// ARBOL GENEAL�GICO    (80)


#include "bintree_eda.h"
#include <iostream>
#include <algorithm> 

using namespace std;

bool arbolGenealogico(const bintree<int>& arbol, int& altura) {
	//Comprobamos los casos base (arbol vacio, ningun hijo, solo hijo derecho)
	//De los cuales todos son v�lidos menos tener el hijo derecho sin el izquierdo
	//Arbol vac�o
	if (arbol.empty()) {
		altura = 0;
		return true;
	}
	//Arbol no vac�o sin hijos
	if (arbol.left().empty() && arbol.right().empty()) {
		altura = 1;
		return true;
	}

	//S�lo hijo derecho, caso no v�lido
	if (arbol.left().empty() && !arbol.right().empty()) {
		return false;
	}


	//Hijo izquierdo  (Se necesita recursi�n)
	if (!arbol.left().empty() && arbol.right().empty()) {
		int alturaIzquierdo;
		bool izquierdoOK;
		//Recursi�n sobre el hijo izquierdo
		izquierdoOK = arbolGenealogico(arbol.left(), alturaIzquierdo);
		//Altura del subarbol del hijo izquierdo mas el nivel actual
		altura = alturaIzquierdo + 1;
		//Si no se cumple la condici�n de mayor de 18
		if (arbol.root() < (arbol.left().root() + 18)) {
			izquierdoOK = false; //La validez del hijo se anula
		}
		return izquierdoOK;
	}

	//Dos hijos (Caso recursivo general)
	//Flags para la correcci�n de cada subarbol
	int alturaIzquierdo, alturaDerecho;
	bool izquierdoOK, derechoOK, arbolOK;
	if (!arbol.left().empty() && !arbol.right().empty()) {
		//Recursi�n sobre el hijo izquierdo
		izquierdoOK = arbolGenealogico(arbol.left(), alturaIzquierdo);
		//Recursi�n sobre el hijo derecho
		derechoOK = arbolGenealogico(arbol.right(), alturaDerecho);
		//cogemos la m�xima altura de los hijos
		altura = max(alturaIzquierdo, alturaDerecho) + 1;
		//El arbol es correcto si sus dos hijos son correctos
		arbolOK = izquierdoOK && derechoOK;
		//Si no se cumple la condici�n de mayor de 18
		if (arbol.root() < (arbol.left().root() + 18) || arbol.root() < (arbol.right().root() + 18)) {
			arbolOK = false; //La validez del arbol se anula
		}
		//Si no se cumple la regla de hijo izquierdo dos a�os mayor que hijo derecho
		if (arbol.left().root() < arbol.right().root() + 2) {
			arbolOK = false; //La validez del arbol se anula
		}


		return arbolOK;
	}

}



//MAIN GENERICO
int main80(int argc, char** args)
{	
	int N, n;
	cin >> N;
	for (n = 0; n < N; n++)
	{
		int res, height;
		bintree<int> tree;
		tree = leerArbol(-1);
		res = arbolGenealogico(tree, height);
		if (res)
			cout << "SI " << height << endl;
		else
			cout << "NO" << endl;
	}
	return 0;
}