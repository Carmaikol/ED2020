//Miguel Aguilera Zorzo  05450952K
//Duplicar una lista enlazada


#include <iostream>
#include "duplicarLista.h"

using namespace std;




bool tratar_caso08() {
	int numeros;
	
	cin >> numeros;
	if (cin.eof()) return false;

	duplicarLista<int> lista;

	while (numeros != 0) {
		lista.push(numeros);
		cin >> numeros;
	} 


	if (!lista.empty()) {
		lista.duplicar();
		lista.mostrar();
	}


	cout << endl;

	return true;
}


int main08(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso08()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}