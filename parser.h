/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Informática
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /* Analizador sintáctico para el ejercicio del lenguaje Calculon++ */

#ifndef __PARSER_H
#define __PARSER_H

#include <iostream>
#include <string>
#include <sstream>
#include "bintree_eda.h"

class Lexer {
public:
	Lexer(std::istream& in) : in(in) { }

	std::string peek() {
		if (next != "") {
			return next;
		}
		else {
			next = read_next();
			return next;
		}
	}

	std::string get() {
		std::string result = peek();
		next = "";
		return result;
	}

private:
	std::istream& in;

	std::string read_next() {
		std::ostringstream result;
		char current;
		in >> current;
		while (iswspace(current)) {
			in >> current;
		}


		if (isdigit(current)) {
			result << current;
			while (isdigit(in.peek())) {
				in >> current;
				result << current;
			}
		}
		else if (isalpha(current)) {
			result << current;
			while (isalnum(in.peek())) {
				in >> current;
				result << current;
			}
		}
		else if (current == '+' || current == '-' || current == '*' || current == ';' || current == '(' || current == ')') {
			result << current;
		}
		else if (current == ':') {
			result << current;
			in >> current;
			if (current != '=') throw std::domain_error("invalid token: :" + current);
			result << current;
		}
		else {
			throw std::domain_error("invalid_token: " + current);
		}

		return result.str();
	}



private:
	std::string next;
};

class Parser {
public:
	Parser(std::istream& in) : l(in) { }

	bintree<std::string> parse() {
		return S();
	}

private:
	Lexer l;

	bool is_identifier(const std::string& token) {
		return (!token.empty() && isalpha(token[0]));
	}

	bool is_identifier_or_number(const std::string& token) {
		return (!token.empty() && isalnum(token[0]));
	}


	bintree<std::string> S() {
		std::string identifier = l.get();
		if (!is_identifier(identifier)) throw std::domain_error("identifier expected, found " + identifier);

		std::string equal = l.get();
		if (equal != ":=") throw std::domain_error(":= expected, found " + equal);

		bintree<std::string> rhs = E();

		std::string semicolon = l.get();
		if (semicolon != ";") throw std::domain_error("; expected, found " + semicolon);

		return { { identifier }, ":=", rhs };
	}

	bintree<std::string> E() {
		bintree<std::string> term = T();
		return Ep(term);
	}

	bintree<std::string> Ep(const bintree<std::string>& left) {
		std::string op = l.peek();
		if (op == "+" || op == "-") {
			l.get();
			bintree<std::string> term = T();
			return Ep({ left, op, term });
		}
		else {
			return left;
		}
	}

	bintree<std::string> T() {
		bintree<std::string> factor = F();
		return Tp(factor);
	}

	bintree<std::string> Tp(const bintree<std::string>& left) {
		std::string op = l.peek();
		if (op == "*") {
			l.get();
			bintree<std::string> factor = F();
			return Tp({ left, "*", factor });
		}
		else {
			return left;
		}
	}

	bintree<std::string> F() {
		std::string next = l.get();
		if (is_identifier_or_number(next)) {
			return { next };
		}
		else if (next == "(") {
			bintree<std::string> inner = E();
			std::string closing = l.get();
			if (closing != ")") throw std::domain_error(") expected, found " + closing);
			return inner;
		}
		else {
			throw std::domain_error("number or identifier expected, found " + next);
		}
	}
};


bintree<std::string> parse(std::istream& in) {
	return Parser(in).parse();
}


#endif

