#ifndef merge_lista_h
#define merge_lista_h

#include "queue_eda.h"
#include <iostream>

using namespace std;

template <class T>
class mergeLista : public queue<T> {
public:
	void mergear(mergeLista<T>& sublista);
	void mostrar();
};

template <class T>
void mergeLista<T>::mergear(mergeLista<T>& sublista) {
	queue<int>::Nodo* actual;
	queue<int>::Nodo* sublistaActual;
	queue<int>::Nodo* aux;
	actual = this->prim;
	sublistaActual = sublista.prim;
	aux = nullptr;

	//Mientras sigan quedando posiciones
	while (actual != nullptr || sublistaActual != nullptr) {
		//Si el puntero a la lista actual es nulo, o el elemento de la sublista es menos que el de la lista actual
		if (actual == nullptr || (sublistaActual != nullptr && actual->elem >= sublistaActual->elem)) {
			//A�adimos el elemento de la sublista
			//Si el nodo auxiliar distinto de null
			if (aux != nullptr)
				aux->sig = sublistaActual; //Apuntamos su siguiente al nodo actual de la sublista
			else
				this->prim = sublistaActual;
			//Actualizamos el nodo auxiliar
			aux = sublistaActual;
			//Avanzamos en la sublista
			sublistaActual = sublistaActual->sig;



		}
		else {
			//En c.c. a�adimos el elemento de la lista
			if (aux != nullptr)
				aux->sig = actual;
			aux = actual;
			actual = actual->sig;
		}



	}

	this->ult = aux;
	this->nelems += sublista.nelems;
	sublista.prim = sublista.ult = nullptr;
	sublista.nelems = 0;




}



template <class T>
void mergeLista<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}












#endif