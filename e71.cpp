//Miguel Aguilera Zorzo  05450952K
//Insertar sublista en lista


#include <iostream>
#include "eliminarPares.h"

using namespace std;




bool tratar_caso71() {
	int num_casos;
	Tiempo numero;
	eliminarPares<Tiempo> lista;

	cin >> num_casos;
	if (cin.eof()) return false;
	if (num_casos == 0) return false;


	for (int i = 0; i < num_casos; i++) {
		cin >> numero;
		lista.push(numero);
	}


	if (!lista.empty()) {
		lista.eliminar();
	}


	if (!lista.empty())
		lista.mostrar();

	cout << endl;

		

	








	return true;
}


int main(int argc, char* args[])
{
#ifndef DOMJUDGE
	//std::ifstream in("sampleF02.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso71()) {}

#ifndef DOMJUDGE
	//std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	//system("PAUSE");
#endif
	return 0;
}