#ifndef viaje_luna_h
#define viaje_luna_h
#include "list_eda.h"

template <class T>
class viajeLuna : public list<T> {
protected:
	using iterador = typename list<T>::iterator;

public:
	template <class Predicate>
	void remove_if(Predicate pred);
};


template <class T>
template <class Predicate>
void viajeLuna<T>::remove_if(Predicate pred) {
	if (this->fantasma->sig == this->fantasma) { //Si la lista es vac�a
		//throw std::length_error("Lista vac�a");  //Lanzamos excepci�n
	}else {                              
		//Iteramos
		iterador it;
		for (it = this->begin(); it != this->end();) {
			if (pred(*it)) { //si se cumple el predicado
				//Borramos el elemento
				it = this->erase(it); //iterador pasa a ser el siguiente
			}else {
				++it; //Pasamos al siguiente elemento
			} 
		
		}

	}
}



#endif