#ifndef mergear_listas_h
#define mergear_listas_h

#include "queue_eda.h"
#include <iostream>

using namespace std;

template <class T>
class mergearListas : public queue<T> {
public:
	void mergear(mergearListas<T>& sublista);
	void mostrar();
};

template <class T>
void mergearListas<T>::mergear(mergearListas<T>& sublista) {
	queue<int>::Nodo* actual;
	queue<int>::Nodo* sublistaActual;
	queue<int>::Nodo* aux;
	actual = this->prim;
	sublistaActual = sublista.prim;
	aux = nullptr;

	//Mientras sigan quedando posiciones
	while (actual != nullptr || sublistaActual != nullptr) {
		//Si el puntero a la lista actual es nulo, o el elemento de la sublista es menos que el de la lista actual
		if (actual == nullptr || (sublistaActual != nullptr && actual->elem >= sublistaActual->elem)) {
			//A�adimos el elemento de la sublista
			//Si el nodo auxiliar distinto de null
			if (aux != nullptr)
				aux->sig = sublistaActual; //Apuntamos su siguiente al nodo actual de la sublista
			//Actualizamos el nodo auxiliar
			aux = sublistaActual;
			//Avanzamos en la sublista
			sublistaActual = sublistaActual->sig;
			
		
		}
		else {
			//En c.c. a�adimos el elemento de la lista
			if (aux != nullptr)
				aux->sig = actual;
			aux = actual;
			actual = actual->sig;
		}


	
	}

	this->nelems += sublista.nelems;
	sublista.prim = sublista.ult = nullptr;
	sublista.nelems = 0;


	

}



template <class T>
void mergearListas<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}












#endif