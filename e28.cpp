/*
 * ---------------------------------------------------
 *                ESTRUCTURAS DE DATOS
 * ---------------------------------------------------
 *              Manuel Montenegro Montes
 *              Facultad de Inform�tica
 *         Universidad Complutense de Madrid
 * ---------------------------------------------------
 */

 /*
 * Comienza poniendo el nombre de los/as componentes del grupo:
 *
 * Estudiante 1: Miguel Aguilera Zorzo
 * Estudiante 2:
 */


#include <iostream>
#include <fstream>
#include "treemap_eda.h"


using namespace std;


bool tratar_caso28() {
	map<string, int> corregidos;
	int numeroNotas;
	//Cogemos el numero de casos
	cin >> numeroNotas;
	cin.ignore(); //Para pasar a la siguiente l�nea
	//Si es 0 paramos el programa
	if (numeroNotas == 0) return false;

	//Recorremos los datos
	for (int i = 0; i < numeroNotas; i++) {
		string nombre, nota;
		getline(cin, nombre);
		getline(cin, nota);
		//Comprobamos que existe en el mapa
		if (!corregidos.count(nombre)) 
			corregidos[nombre] = 0;
			//Si es CORRECTO sumamos 1
		if (nota == "CORRECTO") {
			corregidos[nombre]++;

		}
		//Si es INCORRECTO restamos uno
		else if (nota == "INCORRECTO") {
			corregidos[nombre]--;
		}
		
	}

	//Recorremos los corregidos para imprimir resultado
	for (auto evaluacion: corregidos) {
		//Si el resultado es distinto a 0
		if (evaluacion.valor != 0) {
			//Lo imprimimos
			cout << evaluacion.clave << ", " << evaluacion.valor << endl;
		}
	}

	cout << "---" << endl;


	return true;
}



int main28() {
#ifndef DOMJUDGE
	//std::ifstream in("sample.in");
	//auto cinbuf = std::cin.rdbuf(in.rdbuf());
#endif

	while (tratar_caso28()) {}

#ifndef DOMJUDGE
//	std::cin.rdbuf(cinbuf);
	// Descomentar si se trabaja en Windows
	system("PAUSE");
#endif
	return 0;
}
