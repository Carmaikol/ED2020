#ifndef invertr_lista_h
#define invertr_lista_h


#include "queue_eda.h"
#include <iostream>

using namespace std;

template <class T>
class invertirLista : public queue<T> {
public:
	void invertir();
	void mostrar();
};



template <class T>
void invertirLista<T>::invertir() {
	queue<int>::Nodo* actual;
	queue<int>::Nodo* anterior = nullptr;
	//empezamos desde el principio
	actual = this->prim;
	//Pero el principio pasa a ser el final
	this->ult = this->prim;

	while (actual->sig != nullptr) {
		//guardamos el siguiente
		queue<int>::Nodo* siguiente = actual->sig;
		//El siguiente pasa a ser el anterior
		actual->sig = anterior;
		//Avanzamos las flags
		anterior = actual;
		actual = siguiente;
	}
	//Actualizamos el �ltimo nodo
	actual->sig = anterior;
	//El �ltimo es el primero
	this->prim = actual;

}


template <class T>
void invertirLista<T>::mostrar() {
	queue<int>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}










#endif