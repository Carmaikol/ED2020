#ifndef eliminar_pares_h
#define eliminar_pares_h


#include "queue_eda.h"
#include "tiempo.h"
#include <iostream>

using namespace std;

template <class T>
class eliminarPares : public queue<T> {
public:
	void eliminar();
	void mostrar();



};


template <class T>
void eliminarPares<T>::eliminar() {
	queue<Tiempo>::Nodo* actual = this->prim;
	while (actual->sig != nullptr) {
		//Guardamos el nodo a borrar
		queue<Tiempo>::Nodo* aBorrar = actual->sig;
		//Si su siguiente no es null, lo enlazamos
		if (aBorrar->sig != nullptr) {
			actual->sig = aBorrar->sig;
			actual = actual->sig;
		}
		else {
			//marcamos �ltimo elemento
			actual->sig = nullptr;
		}


		delete(aBorrar);

	
	
	
	}

	this->ult = actual;


}


template <class T>
void eliminarPares<T>::mostrar() {
	queue<Tiempo>::Nodo* actual = this->prim;
	while (actual != nullptr) {
		cout << actual->elem << " ";
		actual = actual->sig;
	}
}



#endif