#ifndef persona_h
#define persona_h

#include <iostream>
#include <string>

using namespace std;

class Persona {
	//Parte p�blica
public:
	//Constructor
	Persona() : nombre("null"), edad(-1) {};
	//constructor con par�metros
	Persona(string _nombre, int _edad) : nombre(_nombre), edad(_edad) {
	};

	//Get nombre
	const string& getNombre() const {
		return this->nombre;
	}
	//Get edad
	const int& getEdad() const {
		return this->edad;
	}

	//Parte privada
private:
	string nombre;
	int edad;

};







#endif
